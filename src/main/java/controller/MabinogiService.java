package controller;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import domain.PageRequester;
import model.Card;
import model.Deck;

import java.io.IOException;

/**
 * Created by Tarvo T on 01-Dec-16.
 */
public class MabinogiService {
	private static PageRequester page = new PageRequester();
	private static ObjectMapper mapper = new ObjectMapper();

	{
		mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
	}


	//http://devcat.nexon.com/duel/us/card?manaThief
	public static Card getCard(String id) {
		String json = page.getResponseBody("http://devcat.nexon.com/api-g/duel/card/" + id + "?lang=en_US");
		try {
			return mapper.readValue(json, Card.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	//http://devcat.nexon.com/api-g/duel/deck/5ff1aa4?lang=en_US
	public static Deck getDeck(String id) {
		String json = page.getResponseBody("http://devcat.nexon.com/api-g/duel/deck/" + id + "?lang=en_US");
		;
		try {
			return mapper.readValue(json, Deck.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
