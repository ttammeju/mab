package model;

import controller.MabinogiService;
import lombok.Data;

import java.util.List;

/**
 * Created by Tarvo T on 01-Dec-16.
 */
@Data
public class Card {
	private String id;
	private String localName;
	private String season;
	private int star;
	private String type;
	private String costType;
	private String costSymbol;
	private int[] cost;
	private int[] attack;
	private int[] life;
	private int[] armor;
	private String[] text;
	private String advice;
	private List<List<String>> price;
}
