package model;

import controller.MabinogiService;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tarvo T on 01-Dec-16.
 */
@Data
public class Deck {
	private String sample;
	private String id;
	private String deck_name;
	private String user_name;
	private List<Card> deck_string;

	public void setDeck_string(List<String> deck_string) {
		this.deck_string = new ArrayList<Card>();
		for (String id : deck_string){
			Card c = MabinogiService.getCard(id);
			this.deck_string.add(c);
		}
	}

	public List<Card> getDeck_string(){
		return this.deck_string;
	}
}
