package domain;

import org.jsoup.Jsoup;

import java.io.IOException;

/**
 * Created by Tarvo T on 01-Dec-16.
 */
public class PageRequester {

	public String getResponseBody(String url) {
		try {
			String content = Jsoup.connect(url).ignoreContentType(true).get().text();
			return content;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
