package domain.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Tarvo T on 01-Dec-16.
 */
public class Database {

	public static void populateDummyData(){
		//TODO dummy data
	}

	public Connection getConnection() throws SQLException {
		String url = "jdbc:postgresql://ec2-54-204-36-183.compute-1.amazonaws.com:5432/" +
				"d5oqbjvp8q2jqv" +
				"?user=jewizzywizygei&" +
				"password=elqbi5pZDOElhrbCrPan9IiAM5&" +
				"ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory";
		return DriverManager.getConnection(url);
	}

}
